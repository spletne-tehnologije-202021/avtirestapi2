const Hapi = require('@hapi/hapi');
const Boom = require('@hapi/boom');

const init = async() => {
    const server = Hapi.server({
        host: 'localhost',
        port: 5000
    });

    server.ext('onRequest', (req, h) => {
        console.log(`Zahteva na: ${req.url.protocol}//${req.url.hostname}:${req.url.port}${req.url.pathname}`);
        return h.continue;
    });

    const dbOptions = {
        url: 'mongodb+srv://<user>:<password>@cluster0.mmhxp.mongodb.net/<database>?retryWrites=true&w=majority',
        settings: {
            useNewUrlParser: true,
            useUnifiedTopology: true
        },
        decorate: true
    };

    await server.register({
        plugin: require('hapi-mongodb'),
        options: dbOptions
    });

    server.route({
        path: '/api/avtomobili',
        method: 'GET',
        handler: async(req, h) => {
            try {
                const avtomobili = await req.mongo.db.collection('avtomobils').find({}).toArray();
                return avtomobili;
            } catch (error) {
                throw Boom.internal(error);
            }
        }
    });

    server.route({
        path: '/api/avtomobili/{id}',
        method: 'GET',
        handler: async(req, h) => {
            try {
                const id = req.params.id;
                const ObjectID = req.mongo.ObjectID;
                const poisci = await req.mongo.db.collection('avtomobils').findOne({ _id: new ObjectID(id) });
                if (!poisci) {
                    return Boom.notFound(`Avto z id = ${req.params.id} ni bil najden`);
                } else {
                    return poisci;
                }

            } catch (error) {

                throw Boom.internal(error);
            }
        }
    });

    server.route({
        path: '/api/avtomobili',
        method: 'POST',
        handler: async(req, h) => {
            try {
                const avto = req.payload;
                const dodan = await req.mongo.db.collection('avtomobils').insertOne(avto);
                return h.response(dodan.ops[0]).header('Location', req.url + '/' + dodan.ops[0]._id);

            } catch (error) {
                throw Boom.internal(error);
            }
        }
    });

    server.route({
        path: '/api/avtomobili/{id}',
        method: 'PUT',
        handler: async(req, h) => {
            try {
                const id = req.params.id;
                const ObjectID = req.mongo.ObjectID;
                const avto = {
                    'znamka': req.payload.znamka,
                    'model': req.payload.model,
                    'letnik': req.payload.letnik,
                    'cena': req.payload.cena
                };
                const poisci = await req.mongo.db.collection('avtomobils').findOne({ _id: new ObjectID(id) });
                if (!poisci) {
                    return Boom.notFound(`Avto z id = ${req.params.id} ni bil najden`);
                } else {
                    if (id !== req.payload._id) {
                        return Boom.badRequest('IDja nista enaka');
                    } else {
                        return await req.mongo.db.collection('avtomobils').updateOne({ _id: new ObjectID(id) }, { $set: avto });
                    }
                }


            } catch (error) {
                throw Boom.internal(error);
            }
        }
    });

    server.route({
        path: '/api/avtomobili/{id}',
        method: 'DELETE',
        handler: async(req, h) => {
            try {
                const id = req.params.id;
                const ObjectID = req.mongo.ObjectID;
                const poisci = await req.mongo.db.collection('avtomobils').findOne({ _id: new ObjectID(id) });
                if (!poisci) {
                    return Boom.notFound(`Avto z id = ${req.params.id} ni bil najden`);
                } else {
                    return await req.mongo.db.collection('avtomobils').deleteOne({ _id: new ObjectID(id) });
                }
            } catch (error) {
                throw Boom.internal(error);
            }
        }
    });

    await server.start();
    console.log(`Strežnik teče in je dostopen na naslovu ${server.info.uri}`);
}

process.on('unhandledRejection', (err) => {
    console.log(err);
    process.exit(1);
})

init();